from datetime import timedelta

import sys
import subprocess
import RPi.GPIO as GPIO

from httplib import OK
from httplib import CREATED
from httplib import BAD_REQUEST
from httplib import INTERNAL_SERVER_ERROR
from httplib import UNPROCESSABLE_ENTITY
from httplib import PRECONDITION_FAILED

from flask import abort
from flask import jsonify
from flask import request
from flask import Flask
app = Flask(__name__)


# Broadcom pin-numbering scheme
GPIO.setmode(GPIO.BCM)

# dynamically managed GPIO configuration
gpio_config = {}

# Allowed GPIO modes
gpio_modes = ['IN', 'OUT']

# Allowed GPIO pins on Raspberry Pi B
gpio_numbers = [4, 17, 27, 22, 10, 9, 11, 7, 8, 25, 24, 23, 18, 15, 14]

# Return codes to GPIO modes
gpio_funcs = {0:  'GPIO.OUT',
              1:  'GPIO.IN',
              40: 'GPIO.SERIAL',
              41: 'GPIO.SPI',
              42: 'GPIO.I2C',
              43: 'GPIO.HARD_PWM',
              -1: 'GPIO.UNKNOWN'}


@app.route("/api/get_uptime", methods=['GET'])
def get_uptime():
    with open('/proc/uptime', 'r') as f:
        uptime_seconds = float(f.readline().split()[0])
        uptime_string = str(timedelta(seconds=uptime_seconds))

        return jsonify(uptime=uptime_string), OK

    abort(INTERNAL_SERVER_ERROR)


@app.route("/api/gpio/add", methods=['POST'])
def gpio_add():
    if not request.json:
        print "not a json request"
        abort(BAD_REQUEST)

    if not 'number' in request.json or not 'mode' in request.json:
        print "request missing required field"
        abort(BAD_REQUEST)

    if not int(request.json['number']) in gpio_numbers:
        abort(UNPROCESSABLE_ENTITY)

    if not str(request.json['mode']) in gpio_modes:
        abort(UNPROCESSABLE_ENTITY)

    if int(request.json['number']) in gpio_config:
        abort(PRECONDITION_FAILED)

    print gpio_config

    # Add GPIO configuration to dictionary
    gpio_config[request.json['number']] = request.json['mode']

    print gpio_config

    return jsonify(), CREATED


@app.route("/api/gpio/<int:pin>/modify", methods=['PUT'])
def gpio_modify(pin):
    if not request.json:
        print "not a json request"
        abort(BAD_REQUEST)

    if not 'number' in request.json or not 'mode' in request.json:
        print "request missing required field"
        abort(BAD_REQUEST)

    if not int(request.json['number']) in gpio_numbers:
        abort(BAD_REQUEST)

    if not str(request.json['mode']) in gpio_modes:
        abort(BAD_REQUEST)

    if not int(request.json['number']) in gpio_config:
        abort(PRECONDITION_FAILED)

    print gpio_config

    # Add GPIO configuration to dictionary
    gpio_config[request.json['number']] = request.json['mode']

    print gpio_config

    return jsonify(), OK


@app.route("/api/gpio/<int:pin>/delete", methods=['DELETE'])
def gpio_delete(pin):
    if not int(pin) in gpio_config:
        abort(PRECONDITION_FAILED)

    print gpio_config

    del gpio_config[pin]

    print gpio_config

    return jsonify(), OK


@app.route("/api/gpio/<int:pin>/info", methods=['GET'])
def gpio_info(pin):
    if pin in gpio_config:
        return jsonify(number=pin, mode=gpio_config[pin]), OK
    else:
        abort(PRECONDITION_FAILED)


@app.route("/api/gpio/<int:pin>/read", methods=['GET'])
def gpio_read(pin):
    try:
        GPIO.setup(int(pin), GPIO.IN)
        if GPIO.input(int(pin)) == True:
            value = 1
        else:
            value = 0

        return jsonify(state=value), OK
    except:
        abort(BAD_REQUEST)


@app.route("/api/gpio/<int:pin>/<action>", methods=['GET'])
def gpio_write(pin, action):
    try:
        GPIO.setup(pin, GPIO.OUT)
        if action == "on":
            GPIO.output(int(pin), GPIO.HIGH)
        elif action == "off":
            GPIO.output(int(pin), GPIO.LOW)
        elif action == "toggle": 
            GPIO.output(int(pin), not GPIO.input(int(pin)))
        else:
            abort(BAD_REQUEST)

        return jsonify(), OK
    except:
        abort(BAD_REQUEST)


if __name__ == "__main__":
    try:
        # Run HTTP server
        app.run(host='0.0.0.0', port=80, debug=True)
    except KeyboardInterrupt:
        # Cleanup all GPIO
         GPIO.cleanup()
